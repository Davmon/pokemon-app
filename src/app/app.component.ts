import { Component } from '@angular/core';
import { PokeService } from './services/poke.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'poke-app';

  public pokemones = [];

  constructor(
      private pokeS: PokeService
  ) {}

  ngOnInit(): void {

    let pokeProvisional = [];
    this.pokeS.consultarPokemones().subscribe(res => {
      console.log('Respuesta obtener pokemones', res);
      pokeProvisional = res.results;

      // Recorro los pokemones para añadir sus detalles
      pokeProvisional.forEach(poke => {
        this.pokeS.obtenerPokeDetalles(poke.url).subscribe(resDetalles => {
          this.pokemones.push(resDetalles);
      });
      console.log(this.pokemones);


      })

    })
  }

}
