import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BarrasComponent } from './shared/barras/barras.component';
import { TablaComponent } from './shared/tabla/tabla.component';
import { VistaComponent } from './pages/vista/vista.component';
import { PokeService } from './services/poke.service';

@NgModule({
  declarations: [
    AppComponent,
    BarrasComponent,
    TablaComponent,
    VistaComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    PokeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
