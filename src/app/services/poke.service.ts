import { Injectable } from '@angular/core';
import { map, retry } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokeService {

  constructor(
    public http: HttpClient
  ) { }

  public consultarPokemones() {

    let url = 'https://pokeapi.co/api/v2/pokemon?limit=20&offset=0';

    return this.http.get(url).pipe(
      map((res: any) => {
        return res;
      }),
      retry(5));
    }
    
    public obtenerPokeDetalles(url: string) {
  
      return this.http.get(url).pipe(
        map((res: any) => {
          return res;
        }),
        retry(5));
  }


}
